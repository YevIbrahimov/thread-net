﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Hubs;


namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {

        private readonly IHubContext<CommentHub> _commentHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper)
		{
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> DeleteComment(CommentDTO commentDto)
        {
            var commentEntity = _mapper.Map<Post>(commentDto);

            _context.Posts.Remove(commentEntity);
            await _context.SaveChangesAsync();

            await _commentHub.Clients.All.SendAsync("DeleteComment");

            return commentDto;
        }
    }
}
