﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly LikeService _likeService;

        public CommentsController(CommentService commentService, LikeService likeService)
        {
            _commentService = commentService;
            _likeService = likeService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreateComment([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }

        [HttpPost("like")]
        public async Task<ActionResult<CommentDTO>> LikeComment([FromBody] NewReactionDTO reaction)
		{
            reaction.EntityId = this.GetUserIdFromToken();
            await _likeService.LikeComment(reaction);
            return Ok();
		}

        [HttpPost("dislike")]
        public async Task<ActionResult<CommentDTO>> DislikeComment([FromBody] NewReactionDTO reaction)
        {
            reaction.EntityId = this.GetUserIdFromToken();
            await _likeService.DislikeComment(reaction);
            return Ok();
        }


        [HttpPost("delete")]
        public async Task<ActionResult<CommentDTO>> DeletePost([FromBody] CommentDTO dto)
        {
            return Ok(await _commentService.DeleteComment(dto));
        }
    }
}