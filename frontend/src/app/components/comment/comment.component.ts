import { Component, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { LikeService } from 'src/app/services/like.service';
import { empty, Observable, Subject } from 'rxjs';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { CommentService } from 'src/app/services/comment.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private likeService: LikeService,
        private authDialogService: AuthDialogService,
        private commentService: CommentService
    ) { }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp as User)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.comment = post));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.comment = post));
    }

    public likeCount() {
        let likeCount = 0;
        this.comment.reactions.forEach(element => {
            if (element.isLike == true) likeCount++;
        });
        return likeCount;
    }

    public dislikeCount() {
        let dislikeCount = 0;
        this.comment.reactions.forEach(element => {
            if (element.isLike == false) dislikeCount++;
        });
        return dislikeCount;
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public deleteComment() {
        if (this.comment.author.id == this.currentUser.id) {

            this.commentService
                .deleteComment(this.comment)
                .pipe(takeUntil(this.unsubscribe$))
        }
        else {
            alert("You can delete only your comment");
        }
    }
}

